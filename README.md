This repository is used to create images used for AWS CodeBuild including Ubuntu Linux, Ansible, and python3.

Some parts of this repository were originally derived from https://github.com/aws/aws-codebuild-docker-images

Copyright (c) 2019 Youi Pty Ltd

License: Amazon Software License - see LICENSE.txt for details.
