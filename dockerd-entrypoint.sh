#!/bin/sh
set -e
set -u

nohup /usr/bin/dockerd \
	--host=unix:///var/run/docker.sock \
	--host=tcp://0.0.0.0:2375 \
	--storage-driver=overlay >/var/log/docker.log 2>&1 &

tries=0
max_tries=10
until docker info >/dev/null 2>&1
do
	tries=$(( $tries + 1 ))
	if [ "$tries" -gt "$max_tries" ]; then
		cat /var/log/docker.log
		echo 'Reached maximum retries trying to connect to internal docker host.' >&2
		exit 1
	fi
	sleep 1
done
