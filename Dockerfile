FROM    ubuntu:bionic

ENV     DEBIAN_FRONTEND=noninteractive

ARG     PKGS="\
ca-certificates \
curl \
docker.io \
git \
iptables \
jq \
python-pip \
python-setuptools \
python-yaml \
python3-pip \
python3-setuptools \
python3-yaml \
software-properties-common \
wget \
"

ARG     MIRROR=http://ap-southeast-2.ec2.archive.ubuntu.com
#ARG     MIRROR="http://azure.archive.ubuntu.com"

RUN     sed -ri \
                -e "s^http://.*archive\.ubuntu\.com^${MIRROR}^" \
                -e "1i deb ${MIRROR}/ubuntu/ bionic-security main restricted universe multiverse\n" \
                /etc/apt/sources.list && \
        apt update && \
        apt dist-upgrade -y && \
        apt install --no-install-recommends -y ${PKGS} && \
        apt autoremove --purge -y && \
        rm -rf /var/lib/apt/lists/*

RUN     add-apt-repository ppa:ansible/ansible && \
        apt update && \
        apt install --no-install-recommends -y ansible && \
        rm -rf /var/lib/apt/lists/*

RUN     pip install awscli boto3 && pip3 install awscli boto3

COPY dockerd-entrypoint.sh /usr/local/bin/dockerd-entrypoint.sh
RUN chmod +x /usr/local/bin/dockerd-entrypoint.sh
